# Creates the directories
mkdir codes
mkdir datasets
mkdir weights

# Dumps the code
cd codes
git clone https://github.com/dontgetdown/STN-4-Lasagne

# Downloads the png MNIST images
cd ..
cd datasets
git clone https://github.com/myleott/mnist_png
cd mnist_png
tar -xvzf mnist_png.tar.gz
cp --recursive mnist_png/testing/ test/
cp --recursive mnist_png/training/ train/
rm --recursive --force mnist_png LICENSE convert_mnist_to_png.py mnist_png.tar.gz README.md .git/
cd ..
mv mnist_png/ original_mnist/
