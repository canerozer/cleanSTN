import cv2
import numpy as np
import os
import random
from datetime import datetime
import timeit
import time


class Dataset_traverser():

	def __init__(self, path):
		self.path = path
		self.train_class_ids = []
		self.test_class_ids = []
		self.train_image_ids = []
		self.test_image_ids = []


	def traverser(self):
		for path, dirs, files in os.walk(self.path):
			for f in files:
				test_index = path.find("test")
				train_index = path.find("train")
				png_index = f.find(".png")
				if test_index != -1:
					self.test_class_ids.append(path[test_index+5])
					self.test_image_ids.append(f[:png_index])
				elif train_index != -1:
					self.train_class_ids.append(path[train_index+6])
					self.train_image_ids.append(f[:png_index])


class File_writer():

	def __init__(self, dim, tt, angles, main_dir, test_image_ids,
	test_class_ids, train_image_ids, train_class_ids, background_image_path, dataset_name):
		self.dim = dim
		self.tt = tt
		self.angles = angles
		if tt == "test/":
			self.size = 10000
			self.image_ids = test_image_ids
			self.class_ids = test_class_ids
		elif tt == "train/":
			self.size = 60000
			self.image_ids = train_image_ids
			self.class_ids = train_class_ids
		self.main_dir = main_dir
		self.background_image_path = background_image_path
		self.dataset_name = dataset_name


	def image_reader(self, i):
		img = cv2.imread(self.main_dir+"/"+"original_mnist/"+self.tt+str(self.class_ids[i])+"/"+str(self.image_ids[i])+".png", cv2.IMREAD_GRAYSCALE)
		rows,cols = img.shape
		return img, rows, cols

	def background_image_reader(self):
		background_image_number = np.random.randint(1,4)
		background_image = cv2.imread(self.background_image_path+str(background_image_number)+".png", cv2.IMREAD_GRAYSCALE)
		return background_image

	def transformer(self, cols, rows, angle, img):
		M = cv2.getRotationMatrix2D((cols/2,rows/2),self.angles[angle],1)
		dst = cv2.warpAffine(img,M,(cols,rows)) #applies the transform
		return M, dst

	def RT_image_creator(self, dst, rows, cols):
		new_img = np.zeros((self.dim, self.dim), np.uint8)
		new_rols, new_cols = new_img.shape
		x_random = np.random.randint(0,new_rols - rows-1)
		y_random = np.random.randint(0,new_cols - cols-1)
		new_img[x_random:x_random+rows,y_random:y_random+cols] = dst
		return new_img

	def background_image_paster(self, new_img, background_image):
		for x in xrange(self.dim):
			for y in xrange(self.dim):
				if new_img[x, y] == 0:
					new_img[x, y] = background_image[x, y]
		return new_img

	def noise_creator(self, new_img):
		for x in xrange(self.dim):
			for y in xrange(self.dim):
				if new_img[x, y] == 0:
					new_img[x, y] = np.random.random_integers(0,255)
		return new_img

	def image_inverted(self, img):
		return 255 - img

	def RT_image_creator_inverted(self, dst, rows, cols):
		new_img = 255*np.ones((self.dim, self.dim), np.uint8)
		new_rols, new_cols = new_img.shape
		x_random = np.random.randint(0,new_rols - rows-1)
		y_random = np.random.randint(0,new_cols - cols-1)
		new_img[x_random:x_random+rows,y_random:y_random+cols] = dst
		return new_img

	def background_image_paster_inverted(self, new_img, background_image):
		for x in xrange(self.dim):
			for y in xrange(self.dim):
				if new_img[x, y] == 255:
					new_img[x, y] = background_image[x, y]
		return new_img

	def noise_creator_inverted(self, new_img):
		for x in xrange(self.dim):
			for y in xrange(self.dim):
				if new_img[x, y] == 255:
					new_img[x, y] = np.random.random_integers(0,255)
		return new_img

	def image_writer_with_background(self):
		for angle in xrange(len(self.angles)):
			for i in xrange(self.size):
				print str(angle*self.size+i)+".png"
				img, rows, cols = self.image_reader(i)
				background_image = self.background_image_reader()
				M, dst = self.transformer(cols, rows, angle, img)
				new_img = self.RT_image_creator(dst, rows, cols)
				new_img = self.background_image_paster(new_img, background_image)
				cv2.imwrite(self.main_dir+"/"+self.dataset_name+"/"+
				self.tt+str(self.class_ids[i])+"/"+str(self.size*int(angle)+
				int(self.image_ids[i]))+".png", new_img)

	def image_writer_with_noise(self):
		for angle in xrange(len(self.angles)):
			for i in xrange(self.size):
				print str(angle*self.size+i)+".png"
				img, rows, cols = self.image_reader(i)
				background_image = self.background_image_reader()
				M, dst = self.transformer(cols, rows, angle, img)
				new_img = self.RT_image_creator(dst, rows, cols)
				new_img = self.noise_creator(new_img)
				cv2.imwrite(self.main_dir+"/"+self.dataset_name+"/"+
				self.tt+str(self.class_ids[i])+"/"+str(self.size*int(angle)+
				int(self.image_ids[i]))+".png", new_img)

	def image_writer_RT(self):
		for angle in xrange(len(self.angles)):
			for i in xrange(self.size):
				print str(angle*self.size+i)+".png"
				img, rows, cols = self.image_reader(i)
				M, dst = self.transformer(cols, rows, angle, img)
				new_img = self.RT_image_creator(dst, rows, cols)
				cv2.imwrite(self.main_dir+"/"+self.dataset_name+"/"+
				self.tt+str(self.class_ids[i])+"/"+str(self.size*int(angle)+
				int(self.image_ids[i]))+".png", new_img)

	def image_writer_with_background_inverted(self):
		for angle in xrange(len(self.angles)):
			for i in xrange(self.size):
				print str(angle*self.size+i)+".png"
				img, rows, cols = self.image_reader(i)
				background_image = self.background_image_reader()
				M, dst = self.transformer(cols, rows, angle, img)
				dst = self.image_inverted(dst)
				new_img = self.RT_image_creator_inverted(dst, rows, cols)
				new_img = self.background_image_paster_inverted(new_img, background_image)
				cv2.imwrite(self.main_dir+"/"+self.dataset_name+"/"+
				self.tt+str(self.class_ids[i])+"/"+str(self.size*int(angle)+
				int(self.image_ids[i]))+".png", new_img)

	def image_writer_with_noise_inverted(self):
		for angle in xrange(len(self.angles)):
			for i in xrange(self.size):
				print str(angle*self.size+i)+".png"
				img, rows, cols = self.image_reader(i)
				M, dst = self.transformer(cols, rows, angle, img)
				dst = self.image_inverted(dst)
				new_img = self.RT_image_creator_inverted(dst, rows, cols)
				new_img = self.noise_creator_inverted(new_img)
				cv2.imwrite(self.main_dir+"/"+self.dataset_name+"/"+
				self.tt+str(self.class_ids[i])+"/"+str(self.size*int(angle)+
				int(self.image_ids[i]))+".png", new_img)
