from dataset_gen_class import Dataset_traverser, File_writer
import cv2
import numpy as np
import os
import random
from datetime import datetime
import timeit
import time

dataset_name = "0_90_45_rotated"
t_start = time.time()
os.chdir(os.pardir)
os.chdir(os.pardir)
ancestor_dir = os.getcwd()
path = ancestor_dir+"/datasets/original_mnist/"
background_image_path = ancestor_dir+"/datasets/"+dataset_name+"/backgrounds/"
os.chdir(path)

dt = Dataset_traverser(path)
dt.traverser()

os.chdir(os.pardir)
main_dir = os.getcwd()

DIM = 60
train_or_test = ["train/", "test/"]
angles = [0, 90, 45]

#fw_test = File_writer(DIM, train_or_test[1], angles, main_dir, dt.test_image_ids, dt.test_class_ids, dt.train_image_ids, dt.train_class_ids, background_image_path, dataset_name)
#fw_test.image_writer_RT()

fw_train = File_writer(DIM, train_or_test[0], angles, main_dir, dt.test_image_ids, dt.test_class_ids, dt.train_image_ids, dt.train_class_ids, background_image_path, dataset_name)
fw_train.image_writer_RT()
