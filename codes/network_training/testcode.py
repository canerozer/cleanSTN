import os
# os.environ['THEANO_FLAGS']='device=cuda0'
import time

import matplotlib
import numpy as np
np.random.seed(123)
import matplotlib.pyplot as plt
import lasagne
import theano
import theano.tensor as T
conv = lasagne.layers.Conv2DLayer
pool = lasagne.layers.MaxPool2DLayer

from lasagne.utils import as_tuple, floatX
from lasagne.random import get_rng
import cv2

NUM_EPOCHS = 101
BATCH_SIZE = 256
LEARNING_RATE = 0.001
DATASET_NAME = "0_90_45_10_rotated"

os.chdir(os.pardir)
os.chdir(os.pardir)
ancestor_dir = os.getcwd()
print ancestor_dir
#INPUT FILE DEGISTIRILECEK

# PARAMETERS

from collections import OrderedDict
from random import random
from itertools import izip

t_start = time.time()
project_dir = ancestor_dir+"/datasets/"+DATASET_NAME
test_dir = ancestor_dir+"/datasets/"+DATASET_NAME+"/test"
train_dir = ancestor_dir+"/datasets/"+DATASET_NAME+"/train"
out_file = ancestor_dir+"/datasets/"+DATASET_NAME+"/"+DATASET_NAME+".npz"
train_k = 5
valid_k = 1
n_classes = 10
NUM_CLASSES = 10
DIM = 60

# COMMENTING STARTS IF THE FILE ALREADY EXISTS

if(os.path.exists(out_file)==False):
    print "numpy array file not found!"
    test_class_ids_list = []
    test_image_ids_list = []
    trainandvalid_class_ids_list = []
    trainandvalid_image_ids_list = []

    for path, dirs, files in os.walk(project_dir):
                for f in files:
                    test_index = path.find("test")
                    train_index = path.find("train")
                    png_index = f.find(".png")
                    if test_index != -1:
                        test_class_ids_list.append(path[test_index+5])
                        test_image_ids_list.append(f[:png_index])
                    elif train_index != -1:
                        trainandvalid_class_ids_list.append(path[train_index+6])
                        trainandvalid_image_ids_list.append(f[:png_index])
    ###
#     print test_class_ids_list[:20]
#     print test_image_ids_list[:20]
#    print trainandvalid_class_ids_list[:20]
#    print trainandvalid_image_ids_list[:20]
    ###   
    
    trainandvalid_size = len(trainandvalid_image_ids_list)
    test_size = len(test_image_ids_list)
    
    ###
#     print trainandvalid_imageandclass_index_list[:20]
    ###
    
    trainandvalid_dict = OrderedDict(izip(trainandvalid_image_ids_list, trainandvalid_class_ids_list))
    trainandvalid_dict = (sorted(trainandvalid_dict.items(),
                                  key = lambda x: random()))
    trainandvalid_dict = OrderedDict(trainandvalid_dict)
    train_dict = OrderedDict(trainandvalid_dict.items()[:trainandvalid_size*train_k/(train_k+valid_k)])
    train_dict_image_ids, train_dict_class_ids = zip(*train_dict.items())
    valid_dict = OrderedDict(trainandvalid_dict.items()[trainandvalid_size*train_k/(train_k+valid_k):])
    valid_dict_image_ids, valid_dict_class_ids = zip(*valid_dict.items())
    ###
#     print "Valid dictionary elements"
#     print valid_dict.keys()[:20]
#     print valid_dict.values()[:20]
#     print valid_dict.items()[:20]
    ###
    
    test_dict = OrderedDict(izip(test_image_ids_list, test_class_ids_list))
    test_dict_image_ids, test_dict_class_ids = zip(*test_dict.items())
    ###
#     print " "
#     print "Test dictionary elements"
#     print test_dict.keys()[:20]
#     print test_dict.values()[:20]
#     print test_dict.items()[:20]
    ###
    
    ###
#     print " "
#     print "Train dictionary elements"
#     print train_dict.keys()[:20]
#     print train_dict.values()[:20]
#     print train_dict.items()[:20]
    ###
    
    test_class_ids = (np.eye(n_classes)[np.asarray(test_dict_class_ids[:], dtype=np.int64)])
    train_class_ids = (np.eye(n_classes)[np.asarray(train_dict_class_ids[:], dtype=np.int64)])
    valid_class_ids = (np.eye(n_classes)[np.asarray(valid_dict_class_ids[:], dtype=np.int64)])

    test_images = np.zeros((test_size, 1, DIM, DIM), dtype=np.float32)
    train_images = np.zeros((trainandvalid_size*train_k/(valid_k+train_k), 1, DIM, DIM), dtype=np.float32)
    valid_images = np.zeros((trainandvalid_size*valid_k/(valid_k+train_k), 1, DIM, DIM), dtype=np.float32)

    print "Reading images is started."
    t_imread = time.time()
    print t_start - t_imread
    
    i=0
    print (test_dir+"/"+test_dict_class_ids[i]+"/"+test_dict_image_ids[i]+".png")
    print (train_dir+"/"+train_dict_class_ids[i]+"/"+train_dict_image_ids[i]+".png")
    print (train_dir+"/"+valid_dict_class_ids[i]+"/"+valid_dict_image_ids[i]+".png")
    
    for i in xrange(test_images.shape[0]):
        if i%1000 == 0:
            print str(i)+"/"+str(test_images.shape[0])+" of test images are complete."
        test_images[i] = cv2.imread(test_dir+"/"+test_dict_class_ids[i]+"/"+test_dict_image_ids[i]+".png", cv2.IMREAD_GRAYSCALE)

    for i in xrange(train_images.shape[0]):
        if i%1000 == 0:
            print str(i)+"/"+str(train_images.shape[0])+" of train images are complete."
        train_images[i] = cv2.imread(train_dir+"/"+train_dict_class_ids[i]+"/"+train_dict_image_ids[i]+".png", cv2.IMREAD_GRAYSCALE)

    for i in xrange(valid_images.shape[0]):
        if i%1000 == 0: 
            print str(i)+"/"+str(valid_images.shape[0])+" of valid images are complete."
        valid_images[i] = cv2.imread(train_dir+"/"+valid_dict_class_ids[i]+"/"+valid_dict_image_ids[i]+".png", cv2.IMREAD_GRAYSCALE)

        
    print "Images will be normalized"
    valid_images[:] = (valid_images - 128)/(128+0.0000001)
    test_images[:] = (test_images - 128)/(128+0.0000001)
    train_images[:] = (train_images - 128)/(128+0.0000001)

    t_normend = time.time()
    print t_normend - t_imread
    print "Saving dictionary"
    dictionary = {'x_train':train_images, 'x_valid':valid_images, 'x_test':test_images, 'y_train':train_class_ids, 
                  'y_valid':valid_class_ids, 'y_test':test_class_ids}

    np.savez(out_file, x_train=dictionary['x_train'], y_train=dictionary['y_train'], x_test=dictionary['x_test'], y_test=dictionary['y_test'], x_valid=dictionary['x_valid'], y_valid=dictionary['y_valid']) 
    t_save_end = time.time()
    print t_save_end - t_normend 

# # COMMENTING END IF THE FILE ALREADY EXISTS

mnist_90_rotated = out_file
# THEN, BY USING A DIFFERENT NUMPY ARRAY DICTIONARY, WE WILL BE LOADING OUR DATA.
def load_data(x):
    data = np.load(x)
    
    X_train, y_train = data['x_train'], np.argmax(data['y_train'], axis=-1)
    X_valid, y_valid = data['x_valid'], np.argmax(data['y_valid'], axis=-1)
    X_test, y_test = data['x_test'], np.argmax(data['y_test'], axis=-1)

    # reshape for convolutions
    X_train = X_train.reshape((X_train.shape[0], 1, DIM, DIM))
    X_valid = X_valid.reshape((X_valid.shape[0], 1, DIM, DIM))
    X_test = X_test.reshape((X_test.shape[0], 1, DIM, DIM))
    
    print "Train samples:", X_train.shape
    print "Validation samples:", X_valid.shape
    print "Test samples:", X_test.shape
    
    return dict(
        X_train=lasagne.utils.floatX(X_train),
        y_train=y_train.astype('int32'),
        X_valid=lasagne.utils.floatX(X_valid),
        y_valid=y_valid.astype('int32'),
        X_test=lasagne.utils.floatX(X_test),
        y_test=y_test.astype('int32'),
        num_examples_train=X_train.shape[0],
        num_examples_valid=X_valid.shape[0],
        num_examples_test=X_test.shape[0],
        input_height=X_train.shape[2],
        input_width=X_train.shape[3],
        output_dim=10,)
data = load_data(mnist_90_rotated)
print time.time() - t_save_end

